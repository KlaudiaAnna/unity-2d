﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour
{
    // game manager
    private GameManager _gameManager = null;
    static private GameManager _gameManagerS = null;
    static public GameManager GameManager { get { return _gameManagerS; } }

    // singleton
    static private Singleton _instance = null;
    static private bool _isQuitting = false;
    static private bool _isInitialized = false;

    // singleton accessor
    static public bool IsValid { get { return !_isQuitting && _instance != null && _isInitialized; } }
    static public Singleton Instance { get { return _instance; } }

    //
    void Awake()
    {
        _instance = this;
        _isQuitting = false;
        _isInitialized = false;

        Create();
    }

    private void Create()
    {
        _gameManager = new GameManager();
        _gameManagerS = _gameManager;
    }

    public void Initialize()
    {
        _gameManager.Initialize();
    }

    void Update()
    {
        if (_isInitialized == false)
        {
            Initialize();
            _isInitialized = true;

            DontDestroyOnLoad(gameObject);

            return;
        }
        
    }

    void OnApplicationQuit()
    {
        if (IsValid == false)
            return;

    }

    void OnDestroy()
    {
        StopAllCoroutines();
    }

    public void Deinitialize()
    {
        _gameManager.Deinitialize();

        _isInitialized = false;
    }

    private void DestroyObjects()
    {
        _gameManager = null;
        _gameManagerS = null;
    }

    void OnDisable()
    {
        _isQuitting = true;

        DestroyObjects();

        // free static instance of singleton
        _instance = null;

        Debug.Log("Singleton Destroyed");
    }
}
