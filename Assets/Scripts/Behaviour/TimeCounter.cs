﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCounter : MonoBehaviour
{
    float time;
    public Text timeCounter;
    public Text highScore;
    public Text currentScore;


    private void Start()
    {
        if (timeCounter != null)
        {
            time = 0;
            timeCounter.text = "Time : " + time.ToString();
        }

        if (highScore != null)
        {
            highScore.text = "High score : " + Singleton.GameManager.higtScore;
        }

        if (currentScore != null)
        {
            currentScore.text = "Current score : " + Singleton.GameManager.curretntScore;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        time += Time.deltaTime;

        if (timeCounter != null)
        {
            timeCounter.text = "Time : " + time.ToString();
        }

        if (highScore != null)
        {
            highScore.text = "High score : " + Singleton.GameManager.higtScore;
        }

        if (currentScore != null)
        {
            currentScore.text = "Current score : " + Singleton.GameManager.curretntScore;
        }
    }
}
