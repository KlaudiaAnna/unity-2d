﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossroadBehaviour : MonoBehaviour
{
    public bool canGoDown = false;
    public bool canGoUp = false;
    public bool canGoRight = false;
    public bool canGoLeft = false;

    private bool ghostReachedPoint = false;
    private Vector3 NextDirection;

    private GameObject ghost;

    private List<possibleDirectionToMove> listOfPossibleDirections = new List<possibleDirectionToMove>();

    private enum possibleDirectionToMove
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    // Use this for initialization
    void Start()
    {
        if (canGoDown)
        {
            listOfPossibleDirections.Add(possibleDirectionToMove.DOWN);
        }

        if (canGoUp)
        {
            listOfPossibleDirections.Add(possibleDirectionToMove.UP);
        }

        if (canGoLeft)
        {
            listOfPossibleDirections.Add(possibleDirectionToMove.LEFT);
        }

        if (canGoRight)
        {
            listOfPossibleDirections.Add(possibleDirectionToMove.RIGHT);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(ghostReachedPoint)
        {
            ghost.GetComponent<GhostMove>().Direction = NextDirection;
            ghostReachedPoint = false;
        }
    }

    void OnTriggerEnter2D(Collider2D co)
    {
        if (co.GetComponent<GhostMove>() == null)
        {
            return;
        }
        ghost = co.gameObject;
        ResolveWhereGhostCameFrom(co);
        int size = listOfPossibleDirections.Count;
        //wydobyć kierunek NextDirection = costam;
    }


    private void ResolveWhereGhostCameFrom(Collider2D co)
    {
        Vector3 dir = this.transform.position - co.transform.position;
        if (dir.x > 0.1)
        {
            this.listOfPossibleDirections.Remove(possibleDirectionToMove.LEFT);
        }
        if (dir.y > 0.1)
        {
            this.listOfPossibleDirections.Remove(possibleDirectionToMove.DOWN);
        }
        if (dir.x > -0.1)
        {
            this.listOfPossibleDirections.Remove(possibleDirectionToMove.RIGHT);
        }
        if (dir.y > -0.1)
        {
            this.listOfPossibleDirections.Remove(possibleDirectionToMove.UP);
        }

        co.GetComponent<GhostMove>().Direction = dir;

    }
    
}
