﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PacmanMove : MonoBehaviour
{
    public float speed = Singleton.GameManager.pacManSpeed;
    Vector2 dest = Vector2.zero;
    Vector2 lastPossition;

    void Start ()
    {
        dest = transform.position;
        Vector2 lastPossition = transform.position;
        SoundManager.PlayConnection("GameScene");

    }

    private void FixedUpdate()
    {
        Vector2 p = Vector2.MoveTowards(transform.position, dest, speed);
        GetComponent<Rigidbody2D>().MovePosition(p);

        if (Input.GetKey(KeyCode.UpArrow) && Valid(Vector2.up))
            dest = (Vector2)transform.position + Vector2.up;
        if (Input.GetKey(KeyCode.RightArrow) && Valid(Vector2.right))
            dest = (Vector2)transform.position + Vector2.right;
        if (Input.GetKey(KeyCode.DownArrow) && Valid(-Vector2.up))
            dest = (Vector2)transform.position - Vector2.up;
        if (Input.GetKey(KeyCode.LeftArrow) && Valid(-Vector2.right))
            dest = (Vector2)transform.position - Vector2.right;
    }

    void Update ()
    {
        //this.transform.rotation = Quaternion.Euler(0, 0, 0);

        if (Input.GetKey(KeyCode.UpArrow) && Valid(Vector2.up))
        {
            GetComponent<Animator>().SetFloat("DirX", 0);
            GetComponent<Animator>().SetFloat("DirY", 1);
        }
        if (Input.GetKey(KeyCode.RightArrow) && Valid(Vector2.right))
        {
            GetComponent<Animator>().SetFloat("DirX", 1);
            GetComponent<Animator>().SetFloat("DirY", 0);
        }
        if (Input.GetKey(KeyCode.DownArrow) && Valid(-Vector2.up))
        {
            GetComponent<Animator>().SetFloat("DirX", 0);
            GetComponent<Animator>().SetFloat("DirY", -1);
        }
        if (Input.GetKey(KeyCode.LeftArrow) && Valid(-Vector2.right))
        {
            GetComponent<Animator>().SetFloat("DirX", -1);
            GetComponent<Animator>().SetFloat("DirY", 0);
        }
    }

    private void LateUpdate()
    {
        Singleton.GameManager.possictionOfPacMan = this.transform.position;
    }

    bool Valid(Vector2 dir)
    {
        Vector2 pos = transform.position;
        RaycastHit2D hit = Physics2D.Linecast(pos, pos + dir);
        bool a = hit.collider == GetComponent<Collider2D>();
        return (hit.collider == GetComponent<Collider2D>());
    }

    private void OnDestroy()
    {
        Singleton.GameManager.curretntScore = 0;
        SceneManager.LoadScene("MenuScene");
    }
}
