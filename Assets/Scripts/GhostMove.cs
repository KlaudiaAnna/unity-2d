﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostMove : MonoBehaviour
{
    public Transform[] waypoint;
    public float speed = Singleton.GameManager.blinkySpeed;
    private int current = 0;

    private bool canMoveUp;
    private bool canMoveDown;
    private bool canMoveLeft;
    private bool canMoveRight;
    private Vector3 direction;

    public Vector3 Direction
    {
        get { return direction; }

        set
        {
            direction = value;
            StopCoroutine("MoveGhostToNextPossition");
            StartCoroutine("MoveGhostToNextPossition",direction);
        }
    }


    // Use this for initialization
    void Start ()
    {
		//inicjacja ruchu
	}

    private void FixedUpdate()
    {
        if(transform.position != waypoint[current].position)
        {
            Vector2 move = Vector2.MoveTowards(transform.position, waypoint[current].position, speed);
            GetComponent<Rigidbody2D>().MovePosition(move);
        }
        else
        {
            if (current == waypoint.Length - 1)
                current = 0;
            else
                current += 1;
        }

        Vector2 dir = waypoint[current].position - transform.position;
        GetComponent<Animator>().SetFloat("DirX", dir.x);
        GetComponent<Animator>().SetFloat("DirY", dir.y);
    }
    
    private void MoveGhostRandomly()
    {
        //RaycastHit2D hit = Physics2D.Linecast(pos , pos + dir);
    }

    void OnTriggerEnter2D(Collider2D co)
    {
        if (co.name == "PacMan")
            Destroy(co.gameObject);
    }

    // Update is called once per frame
    void Update ()
    {


    }

    private IEnumerator MoveGhostToNextPossition(Vector3 dir)
    {
        while (true)
        {
            Vector2 move = Vector2.MoveTowards(this.transform.position, dir, Singleton.GameManager.blinkySpeed);
            this.GetComponent<Rigidbody2D>().MovePosition(move);
            yield return null;
        }
    }



}

/*
bool lastcanMoveUp        =canMoveUp;
bool lastcanMoveLeft      =canMoveDown;
bool lastcanMoveRight     =canMoveLeft;
bool lastcanMoveDown      = canMoveRight;
float radius = this.GetComponent<CircleCollider2D>().radius + 0.05f;
float offset = 0.2f;
Vector3 upRight = this.transform.position + new Vector3(radius, radius, 0);
Debug.DrawLine(upRight, upRight + new Vector3(offset, 0, 0));
Debug.DrawLine(upRight, upRight + new Vector3(0, offset, 0));
RaycastHit2D up1 = Physics2D.Raycast(upRight, upRight + new Vector3(offset, 0, 0));
RaycastHit2D up2 = Physics2D.Raycast(upRight, upRight + new Vector3(0, offset, 0));
Vector3 downLeft = this.transform.position + new Vector3(-radius, -radius, 0);
Debug.DrawLine(downLeft, downLeft + new Vector3(-offset, 0, 0));
Debug.DrawLine(downLeft, downLeft + new Vector3(0, -offset, 0));
RaycastHit2D down1 = Physics2D.Raycast(downLeft, downLeft + new Vector3(-offset, 0, 0));
RaycastHit2D down2 = Physics2D.Raycast(downLeft, downLeft + new Vector3(0, -offset, 0));
Vector3 upLeft = this.transform.position + new Vector3(-radius, radius, 0);
Debug.DrawLine(upLeft, upLeft + new Vector3(-offset, 0, 0));
Debug.DrawLine(upLeft, upLeft + new Vector3(0, offset, 0));
RaycastHit2D right1 = Physics2D.Raycast(upLeft, upLeft + new Vector3(-offset, 0, 0));
RaycastHit2D right2 = Physics2D.Raycast(upLeft, upLeft + new Vector3(0, offset, 0));
Vector3 downRight = this.transform.position + new Vector3(radius, -radius, 0);
Debug.DrawLine(downRight, downRight + new Vector3(offset, 0, 0));
Debug.DrawLine(downRight, downRight + new Vector3(0, -offset, 0));
RaycastHit2D left1 = Physics2D.Raycast(downRight, downRight + new Vector3(offset, 0, 0));
RaycastHit2D left2 = Physics2D.Raycast(downRight, downRight + new Vector3(0, -offset, 0));

if(up1.collider.name == null && up2.collider.name == null)
{
    canMoveUp = true;
}
else
{
    canMoveUp = false;
}

if (down1.collider.name != "maze" && down2.collider.name != "maze")
{
    canMoveDown = true;
}
else
{
    canMoveDown = false;
}

if (right1.collider.name != "maze" && right2.collider.name != "maze")
{
    canMoveRight = true;
}
else
{
    canMoveRight = false;
}

if (left1.collider.name != "maze" && left2.collider.name != "maze")
{
    canMoveLeft = true;
}
else
{
    canMoveLeft = false;
}
*/
